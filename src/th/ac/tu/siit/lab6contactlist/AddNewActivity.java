package th.ac.tu.siit.lab6contactlist;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.*;
import android.widget.*;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class AddNewActivity extends Activity {
	//add new variable
	int position;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_new);
		//Chk if there are extras with the intent
		Intent i = this.getIntent();
		
		
		if(i.hasExtra("position"))
		{
			//Load the value and set them to the widgets
			((EditText)findViewById(R.id.etName)).setText(i.getStringExtra("name"));
			((EditText)findViewById(R.id.etPhone)).setText(i.getStringExtra("phone"));
			((EditText)findViewById(R.id.editMail)).setText(i.getStringExtra("email"));
			//for radio group, we have to do different thing
			RadioGroup rdg = (RadioGroup)findViewById(R.id.rdgType);
			int ptype = Integer.parseInt(i.getStringExtra("type"));
			if(ptype == R.drawable.home)
					rdg.check(R.id.rdHome);
			else
				rdg.check(R.id.rdMobile);
			
			position = i.getIntExtra("position",-1);
			
			
		}
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.add_new, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_save) {
			returnRecord();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	protected void returnRecord() {
		EditText etName = (EditText)findViewById(R.id.etName);
		EditText etPhone = (EditText)findViewById(R.id.etPhone);
		EditText etMail = (EditText)findViewById(R.id.editMail);
		RadioGroup rdgType = (RadioGroup)findViewById(R.id.rdgType);
		
		String sName = etName.getText().toString().trim();
		String sPhone = etPhone.getText().toString().trim();
		String sEmail = etMail.getText().toString().trim();
		//only one can be selected.
		int iType = rdgType.getCheckedRadioButtonId();
		
		if (sName.length() == 0 || sPhone.length() == 0 || iType == -1 || sEmail.length() == 0) {
			AlertDialog dialog = new AlertDialog.Builder(this).create();
			dialog.setTitle("Error");
			dialog.setMessage("All fields are required.");
			dialog.setButton(AlertDialog.BUTTON_NEGATIVE, "OK", 
				new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int which) 
				{}});
			dialog.show();
		}
		//if user create everything :-)
		else {
			Intent data = new Intent();
			data.putExtra("name", sName);
			data.putExtra("phone", sPhone);
			data.putExtra("email", sEmail);
			data.putExtra("position", position);
			
			String sType = "";
			switch(iType) {
			case R.id.rdHome:
				sType = "home";
				break;
			case R.id.rdMobile:
				sType = "mobile";
				break;
			}
			data.putExtra("type", sType);
			this.setResult(RESULT_OK, data);
			this.finish();
			Toast t = Toast.makeText(this, "A new contact added.", 
					Toast.LENGTH_LONG);
			t.show();
		}
	}
}
 